//
//  ContentView.swift
//  swiftuiapplepay
//
//  Created by Sean Kang on 4/22/20.
//  Copyright © 2020 seankang. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
    
            Picker(selection: /*@START_MENU_TOKEN@*/.constant(1)/*@END_MENU_TOKEN@*/, label: Text("Shoes")) {
                Text("Nike").tag(1)
                Text("Puma").tag(2)
            }
            
            Button(action: {
                print("button pressed")
            }) {
                Text("Buy")
            }
            
 

        }
    }
    
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
